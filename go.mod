module gitlab.com/vijayrc/machi

go 1.12

require (
	github.com/aybabtme/rgbterm v0.0.0-20170906152045-cc83f3b3ce59 // indirect
	github.com/buger/goterm v0.0.0-20181115115552-c206103e1f37
	github.com/fatih/color v1.9.0
	github.com/llgcode/draw2d v0.0.0-20200110163050-b96d8208fcfc
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646 // indirect
	github.com/olivere/iterm2-imagetools v1.0.0 // indirect
	github.com/pkg/browser v0.0.0-20180916011732-0a3d74bf9ce4
	github.com/qeesung/image2ascii v1.0.1 // indirect
	github.com/wayneashleyberry/terminal-dimensions v1.0.0
	github.com/yourbasic/graph v0.0.0-20170921192928-40eb135c0b26
)
