# machi
* To draw system diagrams in terminal from text input

## setup
```bash 
$ go mod 
$ go get github.com/fatih/color
$ go get github.com/buger/goterm
$ go install github.com/olivere/iterm2-imagetools/cmd/imgcat #useless as it needs iterm2 that works only on macos
$ go install github.com/olivere/iterm2-imagetools/cmd/imgls #useless as it needs iterm2 that works only on macos
$ go get github.com/pkg/browser
$ go get github.com/llgcode/draw2d
$ go get github.com/qeesung/image2ascii
$ go get github.com/yourbasic/graph

```
---
## links 
* https://github.com/fatih/color 
* https://metacpan.org/pod/release/TELS/Graph-Easy-0.64/bin/graph-easy
* https://github.com/buger/goterm
* https://github.com/olivere/iterm2-imagetools 
* https://github.com/llgcode/draw2d
* https://unix.stackexchange.com/questions/35333/what-is-the-fastest-way-to-view-images-from-the-terminal
* https://github.com/llgcode/draw2d/blob/master/samples/geometry/geometry.go
* https://godoc.org/github.com/pkg/browser
* https://github.com/rivo/tview
* https://developpaper.com/recommend-a-tool-for-making-ascii-flow-chart-graph-easy/
* https://flaviocopes.com/golang-data-structure-graphz

---
## sample 
```
[acro|mainframe,sitep,99% availability,to serve credit data] -> credit-json|https,1K records/s,json
    ->[falcon{to encrypt and publish credit events,runtime:java,tomcat,bluebird}] -> credit-encrypted{pubsub,2k records/s,binary}
    ->[streaming-job{to buffer events into files,gcp-dataproc,gcp}] -> credit{mdb, 10mb/file} -> [gcs]
    ->[preprocessor-job{to convert mdb into avro, spark, dataproc}] -> credit{avro} -> [gcs]
    ->[processor-job{to run models and get scores, spark, dataproc}] -> scores-fff53{avro} -> [gcs]
    ->[publisher-job{to publish results tp pubsub, spark, dataproc}] -> scores-fff53{protobuf} -> [pubsub]; 

[falcon,preprocessor-job,processor-job,publisher-job]<->[kms|to give encryption keys,gcp]    
```
---
## rules 
* See the below system and find rules 
```
                                                   +------------+
                                                   |            |
                                            +----->+            |
                                            |      |     D      |
                                            |      |            |
                                            |      +-----+------+
                                            |            |
                                            |            v
+-------------------+     +-----------------+      +-----+-------+              +-----------------------+              +-----------------+
|                   |     |                ||      |             |              |                       |              |                 |
|                   |     |     B          ||      |     E       |              |                       |              |                 |
|                   +---->+                ++      |             |              |                       +-------------->      H          |
|        A          |     |                |       |             |              |          G            |              |                 |
|                   |     +-------+--------+       +------+------+              |                       |              |                 |
|                   |             |                       |                     |                       |              +-----------------+
|                   |             |                       v       +------------>+                       |
|                   |             |               +-------+-------+             +-------------^---------+
+-------------------+             |               |              ||                           |
                                  |               |        F     ||                           |
                                  |               |              ++                           |
                                  v               |              |                            |
                          +-------+--------+      +--------------+                            |
                          |                |                                                  |
                          |                |                                                  |
                          |       C        |                                                  |
                          |                |                                                  |
                          |                +--------------------------------------------------+
                          +----------------+

```
* Find A with edges out only 
* Find H with edges in only 
* If no(A|H) > 1 then form depth lanes 
* Find the longest path, use a graph lib, make that as the main lane  
* For every box that is not part of the longest path, move them up/down a new depth lane.
* its ok for lines to cross each other, add a ( symbol to show one line jumping over other 
* Form a 2d canvas backed by a 2d empty space char matrix
* **iteration#1** 
    - just do letters for nodes with x,y points according to the above rules 
    - fill up the matrix at points 
* **iteration#2**    
    - do easy straight horizontal and vertical lines e
    - if diagonal, compute horizontal and vertical lines 
* **iteration#3**
    - do boxes 
        - width based on labels chars
        - height based on number of labels 
* **iteration#4**
    - move connecting lines from center to box edges
    - write labels inside boxes 
    






