package trials

import(
	"fmt"
	clr "github.com/fatih/color"
	tm "github.com/buger/goterm"
	"github.com/llgcode/draw2d"
	"github.com/llgcode/draw2d/draw2dimg"
	"github.com/llgcode/draw2d/draw2dkit"
	"image"
	"image/color"
	// "github.com/pkg/browser"
)

//Point ... 
type Point struct{
	X int
	Y int
}

//Trials ...
func Trials(){
	tm.Clear()
	rect(Point{X:10,Y:30},30,6,"system1")
	tm.Flush()
	// png()

}

//Rect ...
func rect(p Point, width int, height int, content string){
	box := tm.NewBox(width|tm.PCT, height, 0)
	fmt.Fprint(box, content)
	tm.Print(tm.MoveTo(box.String(), p.X|tm.PCT, p.Y|tm.PCT))
}

func line(begin Point, end Point){
}

func colors(){
	clr.Cyan("Prints text in cyan.")
	// A newline will be appended automatically
	clr.Blue("Prints %s in blue.", "text")
	// These are using the default foreground colors
	clr.Red("We have red")
	clr.Magenta("And many others ..")

	// Create a new color object
	c := clr.New(clr.FgCyan).Add(clr.Underline)
	c.Println("Prints cyan text with an underline.")

	// Or just add them to New()
	d := clr.New(clr.FgCyan, clr.Bold)
	d.Printf("This prints bold cyan %s\n", "too!.")

	// Mix up foreground and background colors, create new mixes!
	red := clr.New(clr.FgRed)

	boldRed := red.Add(clr.Bold)
	boldRed.Println("This will print text in bold red.")

	whiteBackground := red.Add(clr.BgWhite)
	whiteBackground.Println("Red text with white background.")
}

func png() {
	// Initialize the graphic context on an RGBA image
	dest := image.NewRGBA(image.Rect(0, 0, 800, 600))
	gc := draw2dimg.NewGraphicContext(dest)

	// Set some properties
	gc.SetFillColor(color.RGBA{0x44, 0xff, 0x44, 0xff})
	gc.SetStrokeColor(color.RGBA{0x44, 0x44, 0x44, 0xff})
	gc.SetLineWidth(5)

	// Draw a closed shape
	gc.BeginPath() // Initialize a new path
	gc.MoveTo(10, 10) // Move to a position to start the new path
	gc.LineTo(100, 50)
	gc.QuadCurveTo(100, 10, 10, 10)
	gc.Close()
	gc.FillStroke()
	bubble(gc,50,50,200,200)
	shapes(gc)
	textHello(gc,"")
	// Save to file
	draw2dimg.SaveToPngFile("hello.png", dest)

	// browser.OpenFile("hello.png")
}

// Bubble draws a text balloon.
func bubble(gc draw2d.GraphicContext, x, y, width, height float64) {
	sx, sy := width/100, height/100
	gc.MoveTo(x+sx*50, y)
	gc.QuadCurveTo(x, y, x, y+sy*37.5)
	gc.QuadCurveTo(x, y+sy*75, x+sx*25, y+sy*75)
	gc.QuadCurveTo(x+sx*25, y+sy*95, x+sx*5, y+sy*100)
	gc.QuadCurveTo(x+sx*35, y+sy*95, x+sx*40, y+sy*75)
	gc.QuadCurveTo(x+sx*100, y+sy*75, x+sx*100, y+sy*37.5)
	gc.QuadCurveTo(x+sx*100, y, x+sx*50, y)

	gc.Stroke()
}

func shapes(gc draw2d.GraphicContext){
	draw2dkit.Rectangle(gc,10,10,100,100)
	gc.Stroke()
	draw2dkit.Circle(gc,200,200,50)
	gc.Stroke()
}

func textHello(gc draw2d.GraphicContext, text string) {
	// Draw a rounded rectangle using default colors
	draw2dkit.RoundedRectangle(gc, 5, 5, 135, 95, 10, 10)
	gc.SetLineWidth(0.1)
	gc.FillStroke()

	// Set the font luximbi.ttf
	draw2d.SetFontFolder("/home/vijay/Projects/mine/go/src/github.com/llgcode/draw2d/resource/font/")
	gc.SetFontData(draw2d.FontData{Name: "luxi", Family: draw2d.FontFamilyMono, Style: draw2d.FontStyleNormal})
	// Set the fill text color to black
	gc.SetFillColor(image.Black)
	gc.SetFontSize(12)
	// Display Hello World
	gc.FillStringAt("Hello World", 8, 52)
	gc.SetLineWidth(0.1)
	gc.Stroke()
}
