package logic

import (
	"fmt"
	"math"
	clr "github.com/fatih/color"
	terminal "github.com/wayneashleyberry/terminal-dimensions"
	"strings"
)

//Trials ...
func Trials() {
	//0->1->2
	g := newGraph(3,"system1")
	g.node(0, "loadbalancer", "haproxy", "https")
	g.node(1, "webapp", "tomcat", "https", "java-springboot")
	g.node(2, "customerdb", "mysql", "aws-rds")
	g.edge(0, 1)
	g.edge(1, 2)
	// g.simplePlot()
	g.plot()

	g = newGraph(6,"system2")
	g.node(0,"node0")
	g.node(1,"node1")
	g.node(2,"node2")
	g.node(3,"node3")
	g.node(4,"node4")
	g.node(5,"node5")
	g.edge(0, 1)
	g.edge(1, 2)
	g.edge(1, 3)
	g.edge(2, 4)
	g.edge(3, 4)
	g.edge(4, 5)
	// g.simplePlot()
	g.plot()

	g = newGraph(10,"system3")
	g.node(0,"node0")
	g.node(1,"node1")
	g.node(2,"node2")
	g.node(3,"node3")
	g.node(4,"node4")
	g.node(5,"node5")
	g.node(6,"node6")
	g.node(7,"node7")
	g.node(8,"node8")
	g.node(9,"node9")
	g.edge(0, 1)
	g.edge(0, 2)
	g.edge(1, 2)
	g.edge(2, 3)
	g.edge(2, 4)
	g.edge(3, 5)
	g.edge(5, 6)
	g.edge(3, 7)
	g.edge(4, 8)
	g.edge(7, 8)
	g.edge(8, 9)
	// g.simplePlot()
	g.plot()

}

//Point ...
type Point struct {
	X, Y int
}
//Box ...
type Box struct{
	topX, topY, bottomX, bottomY, leftX, leftY, rightX, rightY int
}
func newBox(p Point, label string) Box{
	padding := (len(label)/2)+2
	rightX,rightY := p.X, p.Y + padding
	leftX,leftY  := p.X,p.Y - padding
	topX,topY := p.X-1, p.Y
	bottomX,bottomY := p.X+1, p.Y
	return Box{topX,topY,bottomX,bottomY,leftX,leftY,rightX,rightY}
}

/* ------------------------------------------------------------------------------ */

//Node ...
type Node struct {
	ID    int
	Edges []int //edges originating out with this node as source
	Name  string
	Data  []string
}

func newNode(id int) *Node {
	return &Node{id, []int{}, "", []string{}}
}

func (node *Node) addEdgeTo(nodeID int) {
	node.Edges = append(node.Edges, nodeID)
}

//check if node edge to given node
func (node *Node) containsEdge(nodeID int) bool {
	for _, edge := range node.Edges {
		if edge == nodeID {
			return true
		}
	}
	return false
}

/* ------------------------------------------------------------------------------ */

//Graph ...
type Graph struct {
	Nodes map[int]*Node
	Name string 
}

func newGraph(sz int, name string) *Graph {
	g := &Graph{Nodes: map[int]*Node{}, Name: name}
	for i := 0; i < sz; i++ {
		g.Nodes[i] = newNode(i)
	}
	return g
}

func (g *Graph) node(i int, name string, data ...string) {
	g.Nodes[i].Name = name
	g.Nodes[i].Data = data
}

func (g *Graph) edge(i, j int) {
	g.Nodes[i].addEdgeTo(j)
}

func (g *Graph) getRNodes() []*Node {
	nodes := []*Node{}
	for _, node := range g.Nodes {
		if len(node.Edges) == 0 {
			nodes = append(nodes, node)
		}
	}
	return nodes
}

func (g *Graph) getNodesFlowingTo(nodeID int) []*Node {
	nodes := []*Node{}
	for _, node := range g.Nodes {
		if node.containsEdge(nodeID) {
			nodes = append(nodes, node)
		}
	}
	return nodes
}

func (g *Graph) simplePlot() {
	for _, node := range g.Nodes {
		fmt.Printf("%d->(", node.ID)
		for _,edge := range node.Edges {
			fmt.Printf("%d,", edge)
		}
		fmt.Printf(")\n")
	}
}

func(g *Graph) getNode(nodeID int)*Node{
	return g.Nodes[nodeID]
}

/* ------------------------------------------------------------------------------ */

//draw canvas
const cX, cY, incr, marginY, marginX = 60, 120, 15, 20, 0 //they should be multiples with incr being the lcd

func getCY()int{
	cY, _ := terminal.Width()
	return int(cY)-10 
}

func (g *Graph) plot() {
	points := map[int]Point{} //node plot hashmap
	nodes := g.getRNodes()
	y := cY-marginY
	var maxX int
	for len(points) < len(g.Nodes) {
		nodes,maxX = plotColumn(g, points, nodes, y)
		y -= incr
	}
	printPoints(g, points, maxX)
}

func plotColumn(g *Graph, points map[int]Point, inputNodes []*Node, y int) ([]*Node, int) {
	x, xInc := marginX, incr
	outputNodes := []*Node{}
	for _, node := range inputNodes {
		_, exists := points[node.ID]
		if !exists {
			points[node.ID] = Point{x, y} //do the right edge
			outputNodes = append(outputNodes, g.getNodesFlowingTo(node.ID)...)
		}
		x += xInc
		
	}
	return outputNodes,x
}

func print(nodes []*Node) {
	for _, node := range nodes {
		fmt.Println(node.ID)
	}
}

func printPoints(g *Graph, points map[int]Point, maxX int) {
	canvas := [cX+1][cY+1]string{}
	//init canvas with .
	for i := 0; i < cX+1; i++ {
		for j := 0; j < cY+1; j++ {
			canvas[i][j] = " "
		}
	}
	//plot edges
	//for each node, get edge node points, so x1,y1->x2,y2
	//v_line: x1->x2,y1 | h_line: x1,y1->y2
	for _, node := range g.Nodes {
		nodePoint := points[node.ID]
		nodeBox := newBox(nodePoint,node.Name)
		x1, y1 := nodePoint.X, nodePoint.Y

		//for every edge from this node
		for _,edge := range node.Edges {
			edgeNode := g.getNode(edge)
			edgePoint := points[edge]
			edgeNodeBox := newBox(edgePoint,edgeNode.Name)
			x2, y2 := edgePoint.X, edgePoint.Y
			// fmt.Printf("%d(%d,%d)->%d(%d,%d)| ", node.ID, x1, y1, edge, x2, y2)

			if(x2 == x1 && y2 > y1) {
				for y := y1; y < y2; y++ { //edge in right 
					if y==edgeNodeBox.leftY-1{
						canvas[x1][y] = "🡺"	
						break
					}else{
						canvas[x1][y] = "."
					}
				}
			} else if (x2 == x1 && y2 < y1) { //edge in left 
				for y := y1; y > y2; y-- {
					if y==edgeNodeBox.rightY{
						canvas[x1][y] = "🡸"	
						break
					}else{
						canvas[x1][y] = "."	
					}
				}
			} else if(x2 > x1 && y2 == y1) { //edge in bottom
				for x := x1; x < x2; x++ {
					if x==edgeNodeBox.topX{
						canvas[x][y1] = "🡻"
						break
					}else{
						canvas[x][y1] = "."
					}
				}
			} else if (x2 < x1 && y2 == y1) { //edge in top 
				for x := x1; x > x2; x-- {
					if x==edgeNodeBox.bottomX{
						canvas[x][y1] = "🡹"
						break
					}else{
						canvas[x][y1] = "."
					}
					
				}
			} else if (x2 > x1 && y2 > y1) { //edge in bottom-right 
				if ifImmediateEdge(y1,y2){ 
					//draw diagonal line 
					for x,y := x1,y1; (x < x2 && y < y2); x,y = x+1,y+1 {
						if x==edgeNodeBox.leftX-1{
							canvas[x][y] = "🡾"
							break
						}else{
							canvas[x][y] = "."
						}
						
					}
				}else{//draw straight lines
					for x:= x1;x<x2;x++{
						canvas[x][y1] = "."		
					}
					for y:= y1;y<y2;y++{
						if y==edgeNodeBox.leftY-1{
							canvas[x2][y] = "🡺"	
							break
						}else {
							canvas[x2][y] = "."		
						}
					}
				}
				
			} else if (x2 > x1 && y2 < y1) { //edge in bottom-left 
				if ifImmediateEdge(y1,y2){ 
					//draw diagonal line 
					for x,y := x1,y1; (x < x2 && y > y2); x,y = x+1,y-1 {
						if x==edgeNodeBox.rightX{
							canvas[x][y] = "🡿"
							break
						}else{
							canvas[x][y] = "."
						}
						
					}
				}else{
					//draw straight lines
					for x:= x1;x<x2;x++{
						canvas[x][y1] = "."		
					}
					for y:= y1;y>y2;y--{
						if y==edgeNodeBox.rightY+1{
							canvas[x2][y] = "🡸"	
							break
						}else {
							canvas[x2][y] = "."		
						}
					}
				}
			} else if (x2 < x1 && y2 < y1) { //edge in top-left 
				if ifImmediateEdge(y1,y2){ 
					//draw diagonal line
					for x,y := x1,y1; (x > x2 && y > y2); x,y = x-1,y-1 {
						if x==edgeNodeBox.bottomX{
							canvas[x][y] = "🡼"
							break
						}else{
							canvas[x][y] = "."
						}
					}
				}else{
					//draw straight lines 
					for x:= x1;x>x2;x--{
						canvas[x][y1] = "."		
					}
					for y:= y1;y>y2;y--{
						if y==edgeNodeBox.rightY+1{
							canvas[x2][y] = "🡸"	
							break
						}else {
							canvas[x2][y] = "."		
						}
					}
				}
			} else { //x2 < x1 && y2 > y1 edge in top-right 
				if ifImmediateEdge(y1,y2){ 
					//draw diagonal line
					for x,y := x1,y1; (x > x2 && y < y2); x,y = x-1,y+1 {
						if x==edgeNodeBox.bottomX{
							canvas[x][y] = "🡽"	
							break
						}else{
							canvas[x][y] = "."
						}
					}
				}else{
					//draw straight lines
					for x:= x1;x>x2;x--{
						canvas[x][y1] = "."		
					}
					for y:= y1;y<y2;y++{
						if y==edgeNodeBox.leftY-1{
							canvas[x2][y] = "🡺"	
							break
						}else {
							canvas[x2][y] = "."		
						}
					}
				}
			}
		}
		//print node name
		nameLength := len(node.Name)
		for i,y := 0,nodeBox.leftY+1;i<nameLength; y,i=y+1,i+1{
			canvas[nodeBox.leftX][y] = string(node.Name[i])
		}
	}

	//plot canvas
	// fmt.Printf("%s\n",clr.CyanString(g.Name))
	systemClr := clr.New(clr.Bold,clr.FgCyan, clr.BgHiBlack).PrintlnFunc()
	systemClr(strings.ToUpper(g.Name))
	nodeClr := clr.New(clr.Bold,clr.FgRed).PrintfFunc()

	for i:=0;i<maxX+1;i++{
		for j:=0;j<cY+1;j++{
			p := canvas[i][j]
			if p == " "{
				fmt.Printf(p)
			}else if p =="."||p=="🡺"||p=="🡸"||p=="🡻"||p=="🡹"||p=="🡾"||p=="🡿"||p=="🡼"||p=="🡽" {
				fmt.Printf(clr.BlueString(p))
			}else{
				nodeClr(p)
			}
		}
		fmt.Println()
	}
	fmt.Println()
}

func ifImmediateEdge(y1,y2 int) bool{
	gap := math.Abs(float64(y2-y1))
	fmt.Println(gap)
	return ( gap == incr)
}